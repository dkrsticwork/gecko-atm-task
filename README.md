# ATM's REST project README


Programmed in 
### Eclipse Oxygen for JavaEE
and 
### openjdk version "1.8.0_151".


Build war and REST api docs with: 
#### mvn clean install


After that offline web based REST api docs path is in:
#### target/jsondoc-ui/jsondoc-ui.html
web page.

Online documentation building with playground building is still in progress.
Tests are run along with mvn build.

Generated war file name is:
#### ingatms-0.0.1-SNAPSHOT.war


Rename it and deploy to Tomcat or any other servlet 3.* container.
Web app. context root changes accordingly.

UI is heavily under construction yet and may be acessible 
by hitting application context root only.

HTTP BasicAuthorization is disabled and may be turned on by
uncommenting code in :
#### rs.gecko.atmsinterview.restserver.BasicAuthFilter.java
method
#### public void filter

This will enable REST api usage only for test/test user.

