package rs.gecko.atmsinterview.tests;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

import rs.gecko.atmsinterview.datastores.LocalModel;
import rs.gecko.atmsinterview.entities.ATM;

public class LocalDatabaseTest {

	private LocalModel nosql = new LocalModel();

	@Test
	public void testCreateATM() {
		ATM atm = createOneATM();
		boolean createResult = nosql.createATM(atm);
		assertTrue("Failed to create ATM", createResult);
		List<ATM> atmsAfterCreation = nosql.selectByCity(atm.getAddress().getCity());
		assertTrue("Database empty after ATM creation", atmsAfterCreation.size()==1);
	}

	@Test
	public void testUpdateATM() {
		ATM atm = createOneATM();
		nosql.createATM(atm);
		atm.setDistance(10000);
		boolean updateResult = nosql.updateATM(atm);
		assertTrue("Failed to update ATM", updateResult);
	}

	@Test
	public void testRemoveATM() {
		ATM atm = createOneATM();
		nosql.createATM(atm);
		long id = atm.getId();
		boolean deleteResult = nosql.deleteATM(atm.getId());
		assertTrue("Failed to remove ATM", deleteResult);
		ATM foundATM = nosql.findById(id);
		assertTrue("ATM not removed", foundATM==null);
	}

	@Test
	public void testSelectByCity() {
		ATM atm = createOneATM();
		boolean createResult = nosql.createATM(atm);
		assertTrue("Failed to create ATM", createResult);
		List<ATM> atmsAfterCreation = nosql.selectByCity(".*");
		assertTrue("Selection failure after ATM creation", atmsAfterCreation.size()==1);
	}

	private ATM createOneATM() {
		String atmJSON = "{\"address\":{\"street\":\"Zuiderhaven\",\"housenumber\":\"8\",\"postalcode\":\"8861 XB\",\"city\":\"Harlingen\",\"geoLocation\":{\"lat\":\"53.174016\",\"lng\":\"5.41296\"}},\"distance\":0,\"type\":\"ING\"}";
		ObjectMapper mapper = new ObjectMapper();
		ATM result = null;
		try {
			result = mapper.readerFor(ATM.class).readValue(atmJSON);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
