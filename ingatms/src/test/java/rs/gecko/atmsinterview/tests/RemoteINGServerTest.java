package rs.gecko.atmsinterview.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;

import org.junit.Test;

import rs.gecko.atmsinterview.datastores.RemoteModel;
import rs.gecko.atmsinterview.entities.ATM;

public class RemoteINGServerTest {

	private RemoteModel ing = new RemoteModel();
	
	@Test
	public void testLoadFromING() {
		try {
			Iterator<ATM> loadedATMs = ing.loadATMIterator();
			assertNotNull("Got null ATMs list", loadedATMs);
			int atmCounter = 0;
			while(loadedATMs.hasNext()) {
				loadedATMs.next();
				atmCounter++;
			}
			assertTrue("Got empty ATMs list", atmCounter > 0);
		}
		catch(Exception e) {
			assertTrue("Got exception while loading ATMs list", false);
		}
	}

}
