package rs.gecko.atmsinterview.restserver;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.annotation.Priority;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class BasicAuthFilter implements ContainerRequestFilter {

	public static final String AUTHENTICATION_HEADER = "Authorization";
	public static final String TEST_USERNAME = "test";
	public static final String TEST_PASSWORD = "test";
	
	private final static char[] ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".toCharArray();

    private static int[]  toInt   = new int[128];

    static {
        for(int i=0; i< ALPHABET.length; i++){
            toInt[ALPHABET[i]]= i;
        }
    }
	
	public void filter(ContainerRequestContext crc) throws IOException {
		// TODO define users/tokens repository and access to resource matrix by path and check here. 
		
		if(!isTestUser(crc))
			crc.abortWith(Response.status(Status.UNAUTHORIZED).build());
	}

	private boolean isTestUser(ContainerRequestContext containerRequest) {
		try {
			String authCredentials = containerRequest.getHeaderString(AUTHENTICATION_HEADER);
			// Here we know where request is heading to.
			String accessPath = containerRequest.getUriInfo().getAbsolutePath().toString();
			return authenticate(authCredentials, accessPath);
		}
		catch(Exception e) {
			return false;
		}
	}
	
	//TODO move these to service .
	public boolean authenticate(String authCredentials, String acessPath) {

		if (null == authCredentials)
			return false;

		final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = decode(encodedUserPassword);
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(
				usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		boolean authenticationStatus = TEST_USERNAME.equals(username)
				&& TEST_PASSWORD.equals(password);
		return authenticationStatus;
	}

	// Java version independent BNase64 decoder for ASCII characters.
	public static byte[] decode(String s){
        int delta = s.endsWith( "==" ) ? 2 : s.endsWith( "=" ) ? 1 : 0;
        byte[] buffer = new byte[s.length()*3/4 - delta];
        int mask = 0xFF;
        int index = 0;
        for(int i=0; i< s.length(); i+=4){
            int c0 = toInt[s.charAt( i )];
            int c1 = toInt[s.charAt( i + 1)];
            buffer[index++]= (byte)(((c0 << 2) | (c1 >> 4)) & mask);
            if(index >= buffer.length){
                return buffer;
            }
            int c2 = toInt[s.charAt( i + 2)];
            buffer[index++]= (byte)(((c1 << 4) | (c2 >> 2)) & mask);
            if(index >= buffer.length){
                return buffer;
            }
            int c3 = toInt[s.charAt( i + 3 )];
            buffer[index++]= (byte)(((c2 << 6) | c3) & mask);
        }
        return buffer;
    } 
	
}
