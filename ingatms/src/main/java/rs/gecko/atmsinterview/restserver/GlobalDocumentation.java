package rs.gecko.atmsinterview.restserver;

import org.jsondoc.core.annotation.global.ApiGlobal;
import org.jsondoc.core.annotation.global.ApiGlobalSection;

@ApiGlobal(
		sections = { 
			@ApiGlobalSection(
				title = "HTTP Verbs", 
				paragraphs = { 
					"Standard POST, PUT, DELETE, GET usage."
				 }
			),
			@ApiGlobalSection(
				title = "Authentication",
				paragraphs = { "HTTP Basic with test un/pw = test/test" }
			),
			@ApiGlobalSection(
				title = "Status codes",
				paragraphs = { "200-ok, 201-created, 204-no content, 500-interval server error" }
			)
		}
	)
public class GlobalDocumentation {}
