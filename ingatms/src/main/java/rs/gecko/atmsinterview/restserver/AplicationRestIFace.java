package rs.gecko.atmsinterview.restserver;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import org.jsondoc.core.annotation.global.ApiGlobal;
import org.jsondoc.core.annotation.global.ApiGlobalSection;

@ApiGlobal(sections={@ApiGlobalSection
	(
		title="Rest interface for simple ATM list CRUD application",
		paragraphs={
			"Documented with: http://jsondoc.org",
			"Base api version v1 path is : /resources/v1"
		}
	)
})
@ApplicationPath("/resources/v1")
public class AplicationRestIFace extends Application {

	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
		s.add(ATMsView.class);
		return s;
	}

}
