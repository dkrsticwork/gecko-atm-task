package rs.gecko.atmsinterview.restserver;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiAuthBasic;
import org.jsondoc.core.annotation.ApiAuthBasicUser;
import org.jsondoc.core.annotation.ApiBodyObject;
import org.jsondoc.core.annotation.ApiError;
import org.jsondoc.core.annotation.ApiErrors;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.annotation.ApiQueryParam;
import org.jsondoc.core.annotation.ApiResponseObject;
import org.jsondoc.core.pojo.ApiVerb;

import rs.gecko.atmsinterview.datastores.LocalModel;
import rs.gecko.atmsinterview.entities.ATM;
import rs.gecko.atmsinterview.rules.ATMsController;

@Api(
	name="AutomaticTellerMachine",
	description="Simple api to CRUD, search and init ATMs locations."
)
@ApiAuthBasic(
	roles={"No roles defined yet."},
	testusers={@ApiAuthBasicUser(username="test", password="test")}
)
@Path("atm")
public class ATMsView {

	private ATMsController atmsRules = new ATMsController();
	private LocalModel localATMRepository = new LocalModel();
	
	@ApiMethod(
			summary="ATM creator",
			description="For supplied valid input creates new ATM in local store.",
			consumes={"application/json"},
			produces={"text/plain"},
			verb={ApiVerb.POST},
			path={"atm/"}
	)
	@ApiBodyObject(clazz=ATM.class)
	@ApiErrors(apierrors={
		@ApiError(code="500", description="If creation fails.")
	})
	@ApiResponseObject(clazz=String.class)
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	public Response createATM(ATM atm) {
		if(localATMRepository.createATM(atm))
			return Response.status(Status.CREATED)
					.entity("ATM created")
					.build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("ATM not created")
					.build();
	}
	
	@ApiMethod(
			summary="ATM updater",
			description="For supplied valid input updates ATM data in local store.",
			consumes={"application/json"},
			produces={"text/plain"},
			verb={ApiVerb.PUT},
			path={"atm/"}
	)
	@ApiBodyObject(clazz=ATM.class)
	@ApiErrors(apierrors={
		@ApiError(code="500", description="If update fails.")
	})
	@ApiResponseObject(clazz=String.class)
	@PUT
	@Consumes({MediaType.APPLICATION_JSON})
	public Response updateATM(ATM atm) {
		if(localATMRepository.updateATM(atm))
			return Response.status(Status.NO_CONTENT)
					.entity("ATM updated")
					.build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Failed to update ATM")
					.build();
	}
	
	@ApiMethod(
			summary="ATM remover",
			description="For supplied atmId removes ATM from local store.",
			consumes={"application/text"},
			produces={"text/plain"},
			verb={ApiVerb.DELETE},
			path={"atm/{atmId}"}
	)
	@ApiErrors(apierrors={
		@ApiError(code="500", description="If deletion fails.")
	})
	@ApiResponseObject(clazz=String.class)
	@DELETE
	@Path("/{atmId}")
	public Response deleteATM(
			@ApiPathParam(name="atmId", description="Numeric long value as identifier od ATM") 
			@PathParam(value="atmId") 
				long atmId) {
		if(localATMRepository.deleteATM(atmId))
			return Response.ok()
					.entity("ATM deleted")
					.build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Failed to delete ATM")
					.build();
	}
	
	@ApiMethod(
		summary="ATM finder",
		description="For supplied atmId finds ATM in local store.",
		consumes={"text/plain"},
		produces={"application/json"},
		verb={ApiVerb.GET},
		path={"atm/{atmId}"}
	)
	@ApiResponseObject(clazz=ATM.class)
	@GET
	@Path("/{atmId}")
	@Produces({MediaType.APPLICATION_JSON})
	public Response findById(
			@ApiPathParam(name="atmId", description="Numeric long value as identifier od ATM") 
			@PathParam(value="atmId") 
				long atmId) {
		return Response.ok(localATMRepository.findById(atmId)).build();
	}
	
	@ApiMethod(
			summary="By city name ATM selector",
			description="For supplied city name start text selects ATMs from local store.",
			consumes={"plain/text"},
			produces= {"application/json"},
			verb={ApiVerb.GET},
			path={"atm?{city}"}
	)
	@ApiResponseObject(clazz=List.class)
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public Response selectByCity(
			@ApiQueryParam(name="city", description="Beginning letters of city name.")
			@QueryParam(value="city") 
			String city) {
		List<ATM> atms = localATMRepository.selectByCity(city);
		GenericEntity<List<ATM>> entity = new GenericEntity<List<ATM>>(atms) {};
		return Response.ok(entity).build();
	}
	
	@ApiMethod(
			summary="Local ATM database initializer",
			description="Initializes local ATM storage from ING online storage.",
			consumes={"text/plain"},
			produces={"text/plain"},
			verb={ApiVerb.POST},
			path={"atm/init"}
	)
	@ApiBodyObject
	@ApiErrors(apierrors={
		@ApiError(code="500", description="If initialization fails.")
	})
	@ApiResponseObject(clazz=String.class)
	@POST
	@Path("/init")
	public Response init() {
		boolean isOK = atmsRules.loadATMsFromINGtoLocal();
		if(isOK)
			return Response.ok().entity("Local ATMs database initialised").build();
		else
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Failed to initialize local ATMs database")
					.build();
	}
	
}
