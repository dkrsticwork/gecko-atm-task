package rs.gecko.atmsinterview.entities;

import java.io.Serializable;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

@ApiObject(
		name="Address",
		description="Address domain object."
)
public class Address implements Serializable {

	private static final long serialVersionUID = 6822644238686901929L;
	
	@ApiObjectField(
			name="street",
			description="ATM street.",
			required=true
	)
	private String street;
	@ApiObjectField(
			name="housenumber",
			description="ATM housenumber.",
			required=true
	)
	private String housenumber;
	@ApiObjectField(
			name="postalcode",
			description="ATM postalcode.",
			required=true
	)
	private String postalcode;
	@ApiObjectField(
			name="city",
			description="ATM city name.",
			required=true
	)
	private String city;
	@ApiObjectField(
			name="geoLocation",
			description="ATM geoLocation.",
			required=true
	)
	private GeoLocation geoLocation;

	public boolean checkValid() {
		return street!=null
				&& !street.isEmpty()
				&& housenumber!=null
				&& !housenumber.isEmpty()
				&& postalcode!=null
				&& !postalcode.isEmpty()
				&& city!=null
				&& !city.isEmpty()
				&& geoLocation!=null 
				&& geoLocation.checkValid();
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getHousenumber() {
		return housenumber;
	}
	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}
	public String getPostalcode() {
		return postalcode;
	}
	public void setPostalcode(String postalcode) {
		this.postalcode = postalcode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public GeoLocation getGeoLocation() {
		return geoLocation;
	}
	public void setGeoLocation(GeoLocation geoLocation) {
		this.geoLocation = geoLocation;
	}
	
}
