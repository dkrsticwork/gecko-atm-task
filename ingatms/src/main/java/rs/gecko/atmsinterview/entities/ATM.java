package rs.gecko.atmsinterview.entities;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import org.dizitart.no2.objects.Id;
import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

@ApiObject(
		name="ATM",
		description="ATM domain object."
)
@XmlRootElement
public class ATM implements Serializable {
	
	private static final long serialVersionUID = 4335451425703494762L;
	
	public static enum TYPE {
		ING
	}
	
	@ApiObjectField(
			name="id",
			description="ATM surrogate identifier for once or more times persisted ATM."
	)
	@Id
	private long id;
	
	@ApiObjectField(
			name="address",
			description="ATM address.",
			required=true
	)
	private Address address;
	@ApiObjectField(
			name="distance",
			// TODO ask if this is distance from bank?
			description="ATM distance from bank?",
			required=true
	)
	private float distance;
	@ApiObjectField(
			name="type",
			description="ATM type.",
			required=true,
			// TODO make dynamic fill from enum
			allowedvalues={"ING"}
	)
	private TYPE type;
	
	public boolean checkValid() {
		return address!=null 
				&& address.checkValid()
				&& distance  >= 0
				&& type!=null;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public float getDistance() {
		return distance;
	}
	public void setDistance(float distance) {
		this.distance = distance;
	}
	public TYPE getType() {
		return type;
	}
	public void setType(TYPE type) {
		this.type = type;
	}
	
}
