package rs.gecko.atmsinterview.entities;

import java.io.Serializable;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

@ApiObject(
		name="GeoLocation",
		description="GeoLocation domain object."
)
public class GeoLocation implements Serializable {

	private static final long serialVersionUID = -8472774224611734231L;
	
	@ApiObjectField(
			name="lat",
			description="ATM lattude.",
			required=true
	)
	private float lat;
	@ApiObjectField(
			name="lng",
			description="ATM longitude.",
			required=true
	)
	private float lng;
	
	public boolean checkValid() {
		return lat >= 0
				&& lng >= 0;
	}
	
	public float getLat() {
		return lat;
	}
	public void setLat(float lat) {
		this.lat = lat;
	}
	public float getLng() {
		return lng;
	}
	public void setLng(float lng) {
		this.lng = lng;
	}
	
}
