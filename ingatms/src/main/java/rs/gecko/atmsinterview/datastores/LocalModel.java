package rs.gecko.atmsinterview.datastores;

import java.util.List;

import org.dizitart.no2.FindOptions;
import org.dizitart.no2.Nitrite;
import org.dizitart.no2.SortOrder;
import org.dizitart.no2.objects.Cursor;
import org.dizitart.no2.objects.ObjectRepository;
import org.dizitart.no2.objects.filters.ObjectFilters;

import rs.gecko.atmsinterview.entities.ATM;

public class LocalModel {
	
	// So far it is in memory usage.
	// TODO change to file storage
	// and handle open/close.
	private static Nitrite nitrite = 
			Nitrite.builder()
			.compressed()
			.openOrCreate();

	private static ObjectRepository<ATM> atmStore = nitrite.getRepository(ATM.class);

	private static long currentId = 0;
	
	private static synchronized long nextId() {
		return ++currentId;
	}
	
	public boolean createATM(ATM atm) {
		if(atm==null || !atm.checkValid())
			return false;
		
		atm.setId(nextId());
		boolean outcome = atmStore.insert(atm).getAffectedCount()==1;
		return outcome;
	}

	public boolean updateATM(ATM atm) {
		if(atm==null || !atm.checkValid())
			return false;
		
		boolean outcome = atmStore.update(ObjectFilters.eq("id", atm.getId()), atm).getAffectedCount()==1;
		return outcome;
	}

	public boolean deleteATM(long atmId) {
		boolean outcome = atmStore.remove(ObjectFilters.eq("id", atmId)).getAffectedCount()==1;
		return outcome;
	}

	public boolean deleteAllATMs() {
		try {
			atmStore.remove(ObjectFilters.gt("id", 0));
			return true;
		}
		catch(Exception e) {
			return false;
		}
	}
	
	public ATM findById(long id) {
		return atmStore.find(ObjectFilters.eq("id", id), FindOptions.limit(0, 1)).firstOrDefault();
	}
	
	public List<ATM> selectByCity(String city) {
		Cursor<ATM> resultCursor = atmStore
				.find(ObjectFilters.regex("address.city", "^" + city) ,
						FindOptions.sort("address.city", SortOrder.Ascending));
		return resultCursor.toList();
	}

}
