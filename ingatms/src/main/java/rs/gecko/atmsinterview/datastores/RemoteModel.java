package rs.gecko.atmsinterview.datastores;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

import rs.gecko.atmsinterview.entities.ATM;

public class RemoteModel {

	private static final String ING_NL_API_LOCATOR_ATMS = "https://www.ing.nl/api/locator/atms/";

	private static final int DIRTY_CHARACTERS_NUMBER = 5;

	private static Logger logger = LoggerFactory.getLogger(RemoteModel.class);

	public Iterator<ATM> loadATMIterator() {
		Iterator<ATM> atmIterator = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			Client client = ClientBuilder.newClient( new ClientConfig());
			WebTarget webTarget = client.target(ING_NL_API_LOCATOR_ATMS);
			
			Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
			Response response = invocationBuilder.get();
			
		    // Buffer and close entity input stream (if any) to prevent
		    // leaking connections (see JERSEY-2157).
			response.bufferEntity();

			if (response.getStatus() != 200) {
				return null;
			}

			InputStream bodyStream = (InputStream)response.getEntity();
			
			// Empty several "dirty" characters from beginning.
			for (int i = 0; i < DIRTY_CHARACTERS_NUMBER; i++) {
				char dirtyChar = (char) bodyStream.read();
				logger.debug("Read " + dirtyChar + " as dirtyChar");
			}
			 
			atmIterator = mapper.readerFor(ATM.class)
					.readValues(bodyStream);
			
			return atmIterator;
		} catch (Exception e) {
			logger.error("In loadFromING()", e);
			// TODO remove when ING server gets back online
			try {
				return mapper.readerFor(ATM.class)
						.readValues(new FileInputStream("/home/dragank/Downloads/atms.json"));
			} catch (Exception e1) {
				return null;
			}
		}
	}

}
