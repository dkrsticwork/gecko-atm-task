package rs.gecko.atmsinterview.rules;

import java.util.Iterator;

import rs.gecko.atmsinterview.datastores.LocalModel;
import rs.gecko.atmsinterview.datastores.RemoteModel;
import rs.gecko.atmsinterview.entities.ATM;

public class ATMsController {

	private LocalModel localATMRepository = new LocalModel();
	private RemoteModel remoteINGRepository = new RemoteModel();
	
	public boolean isValid(ATM atm) {
		return atm!=null && atm.checkValid();
	}
	
	public boolean loadATMsFromINGtoLocal() {
		boolean success = localATMRepository.deleteAllATMs();
		if(success) {
			Iterator<ATM> atmIterator = remoteINGRepository.loadATMIterator();
			while(atmIterator.hasNext()) {
				success &= localATMRepository.createATM(atmIterator.next());
			}
			if(!success) {
				success &= localATMRepository.deleteAllATMs();
			}
		}
		return success;
	}
	
}
