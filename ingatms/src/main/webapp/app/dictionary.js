var bundle_en_US = {
	appName : "ATMs REST interface app UI",
	
	city : "City",
	
	distance : "Distance",
	
	houseNumber : "House number",
	
	lattitude : "Lattitude",
	load : "Load",
	longitude : "Longitude",
	
	noMessages : "No current messages.",
	
	postalCode : "Postal code",
	
	save : "Save",
	search : "Search",
	searchATMs : "Search ATMs",
	street : "Street",
	
	title : "What can we do with these ATMs today?",
	type : "Type"
};

var bundle = bundle_en_US;

const locales = {
	en_US : "en_US",
	sr_RS : "sr_RS"	
};

function setLocale(locale) {
	// TODO switch based on passed locale
	webix.require('', function() {
		bundle = bundle_en_US;
	});
}

function replaceBundleExpressions() {
	// TODO loop over bundle
	var re = /.*#{bundle\.\s+(.*)\s+}*/;
	document.innerHtml.replace(re, bundleValue);
}